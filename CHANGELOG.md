# Change Log

## 0.1.2 (2017-04-29)
### Changes
- Rails 5.1 compatibility

## 0.1.1 (2016-09-16)
### Changes
- Rails 5.0 compatibility

## 0.1.0 (2015-08-13)
### Changes
- Allow to disable/enable logger
